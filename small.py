#!/usr/bin/env python

from slic.core.acquisition import SFAcquisition, DetectorConfig


detectors = DetectorConfig()
detectors.add("JF02T09V03")

daq = SFAcquisition("alvra", "p21739", default_detectors=detectors, append_user_tag_to_data_dir=True, rate_multiplicator=1)


