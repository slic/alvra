import slic.devices as sd

sd.general.smaract.SmarActAxis = sd.general.motor.Motor


shutter = sd.general.shutter.Shutter("SARFE10-OPSH059")

pp = sd.xoptics.pulsepicker.PulsePicker("SAROP11-OPPI112", "SGE-CPCW-71-EVR0:FrontUnivOut15")


phaseShifter = sd.timing.lasertiming.PhaseShifterAramis(
    "SLAAR01-TSPL-EPL",
    "/sf/alvra/config/lasertiming",
#    z_undulator=127,
    name="Experiment laser phase shifter (Globi)"
)

flexTable = sd.endstations.alvra_flex.FlexTable(
    "SARES12-XOTA128",
    name="Flex optical table",
    z_undulator=129
)

prism = sd.general.smaract.SmarActStage(
    "SARES11-XICM125",
    gonio="SARES11-XICM125:ROX1",
    trans="SARES11-XICM125:TRX1",
    rotat="SARES11-XICM125:ROY1",
#    z_undulator=127,
    name="Prime laser prism"
)

xmic = sd.general.smaract.SmarActStage(
    "SARES11-XMI125",
    gonio="SARES11-XMI125:ROY1",
    rotat="SARES11-XMI125:ROZ1",
#    z_undulator=127,
    name="Prime microscope mirror"
)

primeSample = sd.endstations.alvra_huber.Huber(
    "SARES11-XSAM125",
    name="Prime Sample Manipulator",
    z_undulator=125
)

flexSample = sd.endstations.alvra_huber.Huber(
    "SARES12-XSAM128",
    name="Flex Sample Manipulator",
    z_undulator=128
)

#TalbotGratings = sd.general.smaract.SmarActStage(
#    "SARES11-XUSR125-1234",
#    linear_x="SARES11-XUSR125:SA_MOT1",
#    linear_y="SARES11-XUSR125:SA_MOT2",
#    linear_z="SARES11-XUSR125:SA_MOT3",
#    rotation="SARES11-XUSR125:SA_MOT4",
#    name="Talbot Gratings"
#)

#signalMirror = sd.general.smaract.SmarActStage(
#    "SARES11-XUSR125-56",
#    linear="SARES11-XUSR125:SA_MOT5",
#    rotation="SARES11-XUSR125:SA_MOT6",
#    name="Signal Mirror"
#)

#primeMicroscope = sd.endstations.alvra_prime.Microscope(
#    "SARES11-XMI125",
#    gonio="SARES11-CMOV-SMA691113",
#    rotat="SARES11-CMOV-SMA691114",
#    description="Microscope focus and zoom",
#    z_undulator=127
#)

primeTable = sd.endstations.alvra_prime.PrimeTable(
    "SARES11-XOTA125",
    name="Prime optical table",
    z_undulator=127
)

vacuum = sd.endstations.alvra_prime.Vacuum(
    "SARES11-V",
    name="Prime vacuum system",
    z_undulator=127
)

#primeCryTrans = sd.endstations.alvra_prime.VonHamosBragg(
#    "SARES11-XCRY125",
#    z_undulator=127,
#    description="Prime von Hamos X-trans (Bragg)"
#)

laser = sd.loptics.alvra_explaser.ExpLaser()

laserShutter = sd.loptics.lasershutter.LaserShutter(
    "SLAAR11-LDIO-LAS6891:SET_BO02"
)

pbpsOpt = sd.xdiagnostics.intensitymonitor.IntensityMonitorPBPS(
    "SAROP11-PBPS110",
    vme_crate="SAROP11-CVME-PBPS2", # please check this!
    ch_up=11,
    description="Intensity/position monitor in Optics hutch"
)

#pbpsAtt = sd.xdiagnostics.intensitymonitor.IntensityMonitorPBPS(
#    "SAROP11-PBPS122",
#    vme_crate="SAROP11-CVME-PBPS1",
#    ch_up=4, ch_down=5, ch_right=3, ch_left=7,
#    description="Intensity/Position monitor after Attenuator"
#)

screenFE = sd.xdiagnostics.profilemonitor.ProfileMonitorPPRM(
    "SARFE10-PPRM053",
    description="Profile monitor after single-shot spectrometer (PPRM)"
)

screenOP = sd.xdiagnostics.profilemonitor.ProfileMonitorPPRM(
    "SARFE10-PPRM064",
    description="Profile monitor after Front End"
)

screenMirrAlv1 = sd.xdiagnostics.profilemonitor.ProfileMonitorPPRM(
    "SAROP11-PPRM066",
    description="Profile monitor after Alvra Mirror 1 (PPRM)"
)

screenMirrAlv2 = sd.xdiagnostics.profilemonitor.ProfileMonitorPPRM(
    "SAROP11-PPRM078",
    description="Profile monitor after Alvra Mirror 2 (PPRM)"
)

screenMirrorV2 = sd.xdiagnostics.profilemonitor.ProfileMonitorPPRM(
    "SAROP11-PPRM110",
    description="Profile monitor after Vertical Mirror 2 (PPRM)"
)

screenOPEnd = sd.xdiagnostics.profilemonitor.ProfileMonitorPPRM(
    "SAROP11-PPRM117",
    description="Profile monitor after Optics hutch (PPRM)"
)

screenAtt = sd.xdiagnostics.profilemonitor.ProfileMonitorPPRM(
    "SAROP11-PPRM122",
    description="Profile monitor after Attenuator"
)

kbHor = sd.xoptics.kb.KBHor(
    "SAROP11-OKBH124",
    description="Alvra horizontal KB mirror"
)

#kbVer = sd.xoptics.kb.KBVer(
#    "SAROP11-OKBV123",
#    description="Alvra vertical KB mirror"
#)

attFE = sd.xoptics.aramis_attenuator.Attenuator(
    "SARFE10-OATT053",
    description="Attenuator in Front End"
)

attExp = sd.xoptics.aramis_attenuator.Attenuator(
    "SAROP11-OATT120",
    description="Attenuator Alvra"
)

mono = sd.xoptics.dcm.DoubleCrystalMono(
    "SAROP11-ODCM105",
    description="Alvra DCM"
)

offsetmirrorM1 = sd.xoptics.offsetmirrors.OffsetMirror(
    "SAROP11-OOMH064",
    description="Offset Mirror M1"
)

offsetmirrorM2 = sd.xoptics.offsetmirrors.OffsetMirror(
    "SAROP11-OOMH076",
    description="Offset Mirror M2"
)

#monoFEL = sd.xoptics.dcm.CoupledDoubleCrystalMono(
#    "SAROP11-ARAMIS",
#    description="Alvra DCM coupled to FEL energy"
#)

refLaser = sd.xoptics.aramis_reflaser.RefLaser(
    "SAROP11-OLAS120",
    description="Alvra beamline reference laser before KBs (OLAS)"
)

#slitSwitch = sd.xoptics.slits.SlitUnit(
#    "SAROP11-OAPU104",
#    description="Slit in Optics hutch after Photon switchyard and before Alvra mono"
#)

#slitUnd = sd.xoptics.slits.SlitTwinUnit(
#    "SARFE10-OAPU044",
#    description="Slit after Undulator"
#)

slitAttExp = sd.xoptics.slits.SlitUnitCenterWidth(
    "SAROP11-OAPU120",
    description="Slits behind attenuator"
)

#shutUnd = sd.???(
#    "SARFE10-OPSH044",
#    description="Photon shutter after Undulator"
#)

#gasMon = sd.???(
#    "SARFE10-PBIG050",
#    description="Gas Monitor Intensity (PBIG)"
#)

#pbpsUnd = sd.???(
#    "SARFE10-PBPS053",
#    z_undulator=44,
#    description="Intensity position monitor after Undulator (PBPS)"
#)

#shutFE = sd.???(
#    "SARFE10-SBST060",
#    description="Photon shutter in the end of Front End"
#)

#mirrorAlv1 = sd.???(
#    "SAROP11-OOMH064",
#    description="First Alvra Horizontal offset mirror (OMH064)"
#)

#mirrorAlv2 = sd.???(
#    "SAROP11-OOMH076",
#    description="Second Alvra Horizontal offset mirror (OMH076)"
#)

#mirrorV1 = sd.???(
#    "SAROP11-OOMV108",
#    description="Alvra Vertical offset Mirror 1 (OMV108)"
#)

#mirrorV2 = sd.???(
#    "SAROP11-OOMV109",
#    description="Alvra Vertical offset Mirror 2 (OMV109)"
#)

#pulsePicker = sd.???(
#    "SAROP11-OPPI110",
#    description="X-ray pulse picker"
#)

#shutOpt = sd.???(
#    "SAROP11-SBST114",
#    description="Shutter after Optics hutch"
#)

#DelayPALM = sd.???(
#    "SAROP11-PALM118",
#    description="Timing diagnostics THz streaking (PALM)"
#)

#DelayPSEN = sd.???(
#    "SAROP11-PSEN119",
#    description="Timing diagnostics spectral encoding (PSEN)"
#)


